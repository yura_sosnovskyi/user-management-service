package com.sosnovskyi.usermanagement;

import com.sosnovskyi.commondomain.dto.AdminInputDto;
import com.sosnovskyi.commondomain.model.Gender;
import com.sosnovskyi.usermanagement.service.AdminManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication(
		scanBasePackages = {
				"com.sosnovskyi.commondomain",
				"com.sosnovskyi.usermanagement"
		}
)
@EntityScan(basePackages = "com.sosnovskyi.commondomain.model")
@EnableFeignClients(basePackages = "com.sosnovskyi.commondomain.client")
@EnableJpaRepositories
@EnableWebSecurity
@EnableJpaAuditing
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class UserManagementServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserManagementServiceApplication.class, args);
	}

//	@Bean
//	public CommandLineRunner commandLineRunner() {
//		return new CommandLineRunner() {
//			@Autowired
//			private AdminManagementService adminManagementService;
//
//			@Override
//			public void run(String... args) {
//				if (adminManagementService.findAllAdmins().isEmpty()) {
//					adminManagementService.saveAdmin(
//							new AdminInputDto("admin",
//									"firstAdmin",
//									"lastAdmin",
//									"0931112233",
//									"odmen",
//									"admin",
//									"Lviv",
//									21,
//									Gender.MALE));
//				}
//			}
//		};
//	}

}
