package com.sosnovskyi.usermanagement.service;

import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.service.CommonUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserManagementService extends CommonUserService {

    UserOutputDto updateUser(Long id, UserInputDto user);

    UserOutputDto addToLastVisited(Long id, Specialist specialist);

    void deleteUser(Long id);

    UserOutputDto getUser(String email);

    List<UserOutputDto> findAllUsers();

    User findById(Long id);

    void throwIfExist(String email);
}
