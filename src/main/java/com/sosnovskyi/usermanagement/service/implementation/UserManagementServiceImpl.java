package com.sosnovskyi.usermanagement.service.implementation;

import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.commondomain.exception.UserAlreadyExistsException;
import com.sosnovskyi.commondomain.exception.UserNotFoundException;
import com.sosnovskyi.commondomain.mapper.UserMapper;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.usermanagement.repository.UserManagementRepository;
import com.sosnovskyi.usermanagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.sosnovskyi.commondomain.mapper.UserMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.UserMapper.toOutputDto;


@Service
public class UserManagementServiceImpl implements UserManagementService {

    private final UserManagementRepository userManagementRepository;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public UserManagementServiceImpl(UserManagementRepository userManagementRepository) {
        this.userManagementRepository = userManagementRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public UserOutputDto updateUser(Long id, UserInputDto userDto) {
        User oldUser = findById(id);
        if (!oldUser.getEmail().equals(userDto.getEmail())) {
            throwIfExist(userDto.getEmail());
        }

        User user = fromInputDto(userDto, oldUser.getRoles());
        user.setId(id);
        user.setEncodedPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setLastVisitedSpecialists(oldUser.getLastVisitedSpecialists());
        user.setCreatedAt(oldUser.getCreatedAt());
        user.setCreatedBy(oldUser.getCreatedBy());

        return toOutputDto(userManagementRepository.save(user));
    }

    @Override
    public UserOutputDto addToLastVisited(Long id, Specialist specialist) {
        User user = findById(id);
        user.getLastVisitedSpecialists().add(specialist);
        return toOutputDto(userManagementRepository.save(user));
    }

    @Override
    public void deleteUser(Long id) {
        List<User> visitedMe = userManagementRepository.findByLastVisitedSpecialistsId(id);
        visitedMe.forEach(user -> {
            user.getLastVisitedSpecialists()
                    .removeIf(specialist -> specialist.getId().equals(id));
            userManagementRepository.save(user);
        });
        userManagementRepository.deleteById(id);
    }

    @Override
    public UserOutputDto getUser(String email) {
        return userManagementRepository.findByEmail(email)
                .map(UserMapper::toOutputDto)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    @Override
    public List<UserOutputDto> findAllUsers() {
        return userManagementRepository.findAll().stream()
                .map(UserMapper::toOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public User findById(Long id) {
        return userManagementRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id.toString()));
    }

    @Override
    public void throwIfExist(String email) {
        if (userManagementRepository.existsByEmail(email)) {
            throw new UserAlreadyExistsException(email);
        }
    }
}
