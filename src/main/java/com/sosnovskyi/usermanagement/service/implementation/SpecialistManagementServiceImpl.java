package com.sosnovskyi.usermanagement.service.implementation;

import com.sosnovskyi.commondomain.dto.FeedbackInputDto;
import com.sosnovskyi.commondomain.dto.FeedbackOutputDto;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.exception.UserNotFoundException;
import com.sosnovskyi.commondomain.mapper.SpecialistMapper;
import com.sosnovskyi.commondomain.model.Category;
import com.sosnovskyi.commondomain.model.Feedback;
import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.security.util.SecurityUtil;
import com.sosnovskyi.commondomain.service.util.PaginatorUtil;
import com.sosnovskyi.usermanagement.repository.CategoryManagementRepository;
import com.sosnovskyi.usermanagement.repository.SpecialistManagementRepository;
import com.sosnovskyi.usermanagement.service.SpecialistManagementService;
import com.sosnovskyi.usermanagement.service.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.sosnovskyi.commondomain.mapper.FeedbackMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.FeedbackMapper.toOutputDto;
import static com.sosnovskyi.commondomain.mapper.SpecialistMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.SpecialistMapper.toOutputDto;


@Service
@Transactional
public class SpecialistManagementServiceImpl implements SpecialistManagementService {

    private final SpecialistManagementRepository specialistManagementRepository;
    private final UserManagementService userManagementService;
    private final CategoryManagementRepository categoryManagementRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public SpecialistManagementServiceImpl(SpecialistManagementRepository specialistManagementRepository,
                                           UserManagementService userManagementService,
                                           CategoryManagementRepository categoryManagementRepository) {
        this.specialistManagementRepository = specialistManagementRepository;
        this.userManagementService = userManagementService;
        this.categoryManagementRepository = categoryManagementRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public SpecialistOutputDto getSpecialist(Long id) {
        Specialist specialist = specialistManagementRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id.toString()));

        Long currentUserId = SecurityUtil.getAuthenticatedUser().getId();
        if (!specialist.getId().equals(currentUserId)) {
            userManagementService.addToLastVisited(currentUserId, specialist);
        }

        return toOutputDto(specialist);
    }

    @Override
    public SpecialistOutputDto updateSpecialist(Long id, SpecialistInputDto specialistInputDto) {
        Specialist oldSpecialist = findById(id);
        if (!oldSpecialist.getEmail().equals(specialistInputDto.getEmail())) {
            userManagementService.throwIfExist(specialistInputDto.getEmail());
        }

        Specialist specialist = fromInputDto(specialistInputDto,
                oldSpecialist.getRoles(),
                getCategories(specialistInputDto.getCategoryIds()));

        specialist.setId(id);
        specialist.setEncodedPassword(passwordEncoder.encode(specialistInputDto.getPassword()));
        specialist.setRating(oldSpecialist.getRating());
        specialist.setIsVerified(oldSpecialist.getIsVerified());
        specialist.setFeedbacks(oldSpecialist.getFeedbacks());
        specialist.setCreatedAt(oldSpecialist.getCreatedAt());
        specialist.setCreatedBy(oldSpecialist.getCreatedBy());

        return toOutputDto(specialistManagementRepository.save(specialist));
    }

    @Override
    public SpecialistOutputDto verifySpecialist(Long id) {
        Specialist specialist = findById(id);
        specialist.setIsVerified(true);
        return toOutputDto(specialistManagementRepository.save(specialist));
    }

    @Override
    public FeedbackOutputDto addFeedback(FeedbackInputDto feedbackInputDto) {
        Specialist specialist = findById(feedbackInputDto.getSpecialistId());
        User reviewer = userManagementService.findById(feedbackInputDto.getReviewerId());

        Feedback feedback = fromInputDto(feedbackInputDto, specialist, reviewer);

        int oldFeedbackCount = specialist.getFeedbacks().size();
        BigDecimal newRating = specialist.getRating()
                .multiply(BigDecimal.valueOf(oldFeedbackCount))
                .add(BigDecimal.valueOf(feedbackInputDto.getRating()))
                .divide(BigDecimal.valueOf(oldFeedbackCount + 1), RoundingMode.HALF_UP);
        specialist.setRating(newRating);
        specialist.getFeedbacks().add(feedback);

        List<Feedback> feedbackList = specialistManagementRepository.save(specialist)
                .getFeedbacks();

        return toOutputDto(feedbackList.get(oldFeedbackCount));
    }

    @Override
    public void deleteSpecialist(Long id) {
        userManagementService.deleteUser(id);
    }

    @Override
    public List<SpecialistOutputDto> findAllSpecialists() {
        return specialistManagementRepository.findAll().stream()
                .map(SpecialistMapper::toOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public Page<SpecialistOutputDto> findAllUnverifiedSpecialists(Map<String, String> pagingParams) {
        Pageable pageable = PaginatorUtil.createPageable(pagingParams);
        return specialistManagementRepository.findAllByIsVerifiedIsFalse(pageable)
                .map(SpecialistMapper::toOutputDto);
    }

    @Override
    public Specialist findById(Long id) {
        return specialistManagementRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id.toString()));
    }

    private List<Category> getCategories(List<Long> categoryIds) {
        return categoryIds.stream()
                .map(id -> categoryManagementRepository.findById(id).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
