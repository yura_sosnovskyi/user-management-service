package com.sosnovskyi.usermanagement.service.implementation;

import com.sosnovskyi.commondomain.dto.AdminInputDto;
import com.sosnovskyi.commondomain.dto.AdminOutputDto;
import com.sosnovskyi.commondomain.exception.UserAlreadyExistsException;
import com.sosnovskyi.commondomain.exception.UserNotFoundException;
import com.sosnovskyi.commondomain.mapper.AdminMapper;
import com.sosnovskyi.commondomain.model.Admin;
import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.usermanagement.repository.AdminManagementRepository;
import com.sosnovskyi.usermanagement.repository.RoleManagementRepository;
import com.sosnovskyi.usermanagement.repository.UserManagementRepository;
import com.sosnovskyi.usermanagement.service.AdminManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.sosnovskyi.commondomain.mapper.AdminMapper.fromInputDto;
import static com.sosnovskyi.commondomain.mapper.AdminMapper.toOutputDto;


@Service
public class AdminManagementServiceImpl implements AdminManagementService {

    private final AdminManagementRepository adminManagementRepository;
    private final UserManagementRepository userManagementRepository;
    private final RoleManagementRepository roleManagementRepository;
    private final PasswordEncoder passwordEncoder;


    @Autowired
    public AdminManagementServiceImpl(AdminManagementRepository adminManagementRepository,
                                      UserManagementRepository userManagementRepository,
                                      RoleManagementRepository roleManagementRepository) {
        this.adminManagementRepository = adminManagementRepository;
        this.userManagementRepository = userManagementRepository;
        this.roleManagementRepository = roleManagementRepository;
        this.passwordEncoder = new BCryptPasswordEncoder(10);
    }

    @Override
    public AdminOutputDto saveAdmin(AdminInputDto adminInputDto) {
        throwIfExist(adminInputDto.getEmail());
        Admin admin = fromInputDto(adminInputDto, List.of(
                getOrCreateRole("USER"),
                getOrCreateRole("ADMIN")));
        admin.setEncodedPassword(passwordEncoder.encode(adminInputDto.getPassword()));
        return AdminMapper.toOutputDto(userManagementRepository.save(admin));
    }

    @Override
    public AdminOutputDto updateAdmin(Long id, AdminInputDto adminInputDto) {
        Admin oldAdmin = findById(id);
        if (!oldAdmin.getEmail().equals(adminInputDto.getEmail())) {
            throwIfExist(adminInputDto.getEmail());
        }

        Admin admin = fromInputDto(adminInputDto, oldAdmin.getRoles());
        admin.setId(id);
        admin.setCreatedAt(oldAdmin.getCreatedAt());
        admin.setCreatedBy(oldAdmin.getCreatedBy());

        return toOutputDto(adminManagementRepository.save(admin));
    }

    @Override
    public List<AdminOutputDto> findAllAdmins() {
        return adminManagementRepository.findAll().stream()
                .map(AdminMapper::toOutputDto)
                .collect(Collectors.toList());
    }

    @Override
    public void deleteAdmin(Long id) {
        adminManagementRepository.deleteById(id);
    }

    @Override
    public AdminOutputDto getAdmin(String email) {
        return adminManagementRepository.findByEmail(email)
                .map(AdminMapper::toOutputDto)
                .orElseThrow(() -> new UserNotFoundException(email));
    }

    private Role getOrCreateRole(String roleName) {
        return roleManagementRepository.findByName(roleName)
                .orElse(new Role(roleName));
    }

    @Override
    public Admin findById(Long id) {
        return adminManagementRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id.toString()));
    }

    private void throwIfExist(String email) {
        if (userManagementRepository.existsByEmail(email)) {
            throw new UserAlreadyExistsException(email);
        }
    }
}
