package com.sosnovskyi.usermanagement.service.implementation;


import com.sosnovskyi.commondomain.dto.CategoryInputDto;
import com.sosnovskyi.commondomain.dto.CategoryOutputDto;
import com.sosnovskyi.commondomain.model.Category;
import com.sosnovskyi.commondomain.service.util.PaginatorUtil;
import com.sosnovskyi.usermanagement.repository.CategoryManagementRepository;
import com.sosnovskyi.usermanagement.service.CategoryManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class CategoryManagementServiceImpl implements CategoryManagementService {

    private final CategoryManagementRepository categoryManagementRepository;

    @Autowired
    public CategoryManagementServiceImpl(CategoryManagementRepository categoryManagementRepository) {
        this.categoryManagementRepository = categoryManagementRepository;
    }

    @Override
    public CategoryOutputDto findDtoById(Long id) {
        return toOutputDto(findById(id));
    }

    @Override
    public Category findById(Long id) {
        return categoryManagementRepository.findById(id)
                .orElseThrow(() ->
                        new NoSuchElementException(String.format("Category with id=%d does not exist", id))
                );
    }

    @Override
    public Page<CategoryOutputDto> findAll(Map<String, String> params) {
        Pageable pageable = PaginatorUtil.createPageable(params);
        log.info("Category findAll() sorted by {} on pageNumber {} with pageSize {}",
                pageable.getSort(), pageable.getPageNumber(), pageable.getPageSize());

        Page<Category> categories = categoryManagementRepository.findAll(pageable);
        return categories.map(this::toOutputDto);
    }

    @Override
    public Page<CategoryOutputDto> findRootParents(Map<String, String> params) {
        Pageable pageable = PaginatorUtil.createPageable(params);
        log.info("Category findRootParents() sorted by {} on pageNumber {} with pageSize {}",
                pageable.getSort(), pageable.getPageNumber(), pageable.getPageSize());

        Page<Category> categories = categoryManagementRepository.findCategoriesByParentCategoryIsNull(pageable);
        return categories.map(this::toOutputDto);
    }

    @Override
    public boolean existsByName(String name) {
        log.info("Category existsByName() by name: {}", name);
        return categoryManagementRepository.existsByName(name);
    }

    @Override
    public void deleteCategory(Long id) {
        log.info("Category deleteCategory() by id: {}", id);
        Category category = findById(id);
        Optional.ofNullable(category)
                .map(Category::getSpecialists)
                .ifPresent(specialists ->
                        specialists.forEach(specialist -> {
                            List<Category> categories = specialist.getCategories();
                            categories.remove(category);
                            if (category.getParentCategory() != null
                                    && !categories.contains(category.getParentCategory())) {
                                categories.add(category.getParentCategory());
                            }
                            specialist.setCategories(categories);
                        })
                );
        categoryManagementRepository.deleteById(id);
    }

    @Override
    public CategoryOutputDto createCategory(CategoryInputDto categoryInputDto) {
        log.info("Category createCategory()");
        Category category = fromInputDto(categoryInputDto);
        return toOutputDto(categoryManagementRepository.save(category));
    }

    @Override
    @Transactional
    public CategoryOutputDto updateCategory(Long id, CategoryInputDto categoryInputDto) {
        log.info("Category updateCategory() with id: {}", id);

        Category oldCategory = findById(id);
        Category newCategory = fromInputDto(categoryInputDto);
        newCategory.setId(id);

        Long newParentCategoryId = categoryInputDto.getParentCategoryId();
        Category oldParentCategory = oldCategory.getParentCategory();

        if (oldParentCategory != null
                && !oldParentCategory.getId().equals(newParentCategoryId)) {
            oldParentCategory.getSubCategories().remove(oldCategory);
            categoryManagementRepository.save(oldParentCategory);
        }

        newCategory.setSubCategories(new ArrayList<>(oldCategory.getSubCategories()));
        return toOutputDto(categoryManagementRepository.save(newCategory));
    }

    @Override
    public Optional<Category> findByName(String name) {
        log.info("Category findByName() by name: {}", name);
        return categoryManagementRepository.findByName(name);
    }

    @Override
    public void deleteAll() {
        categoryManagementRepository.deleteAll();
    }

    public Category fromInputDto(CategoryInputDto categoryInputDto) {

        Category category = Category.builder()
                .name(categoryInputDto.getName())
                .description(categoryInputDto.getDescription())
                .build();

        List<Category> subCategories =
                Optional.ofNullable(categoryInputDto.getSubCategories())
                        .stream()
                        .flatMap(Collection::stream)
                        .map(this::fromInputDto)
                        .peek(c -> c.setParentCategory(category))
                        .collect(Collectors.toList());

        category.setSubCategories(subCategories);

        Optional.ofNullable(categoryInputDto.getParentCategoryId())
                .ifPresent(id -> category.setParentCategory(findById(id)));

        return category;
    }

    public CategoryOutputDto toOutputDto(Category category) {
        StringBuilder fullPath = new StringBuilder("." + category.getId());

        Category parentCategory = category.getParentCategory();
        Long parentId = Optional.ofNullable(parentCategory)
                .map(Category::getId)
                .orElse(null);

        while (parentCategory != null) {
            fullPath.insert(0, "." + parentCategory.getId());
            parentCategory = parentCategory.getParentCategory();
        }

        List<CategoryOutputDto> subCategoriesDto =
                category.getSubCategories().stream()
                        .map(this::toOutputDto)
                        .collect(Collectors.toList());

        return CategoryOutputDto.builder()
                .id(category.getId())
                .name(category.getName())
                .description(category.getDescription())
                .parentCategoryId(parentId)
                .subCategories(subCategoriesDto)
                .fullPath(fullPath.toString())
                .createdAt(category.getCreatedAt())
                .updatedAt(category.getUpdatedAt())
                .build();
    }
}
