package com.sosnovskyi.usermanagement.service;

import com.sosnovskyi.commondomain.dto.FeedbackInputDto;
import com.sosnovskyi.commondomain.dto.FeedbackOutputDto;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.service.CommonSpecialistService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface SpecialistManagementService extends CommonSpecialistService {

    SpecialistOutputDto getSpecialist(Long id);

    SpecialistOutputDto updateSpecialist(Long id, SpecialistInputDto specialistInputDto);

    SpecialistOutputDto verifySpecialist(Long id);

    FeedbackOutputDto addFeedback(FeedbackInputDto feedbackInputDto);

    void deleteSpecialist(Long id);

    List<SpecialistOutputDto> findAllSpecialists();

    Page<SpecialistOutputDto> findAllUnverifiedSpecialists(Map<String, String> pagingParams);

    User findById(Long id);
}
