package com.sosnovskyi.usermanagement.service;

import com.sosnovskyi.commondomain.dto.CategoryInputDto;
import com.sosnovskyi.commondomain.dto.CategoryOutputDto;
import com.sosnovskyi.commondomain.model.Category;
import org.springframework.data.domain.Page;

import java.util.Map;
import java.util.Optional;

public interface CategoryManagementService {
    CategoryOutputDto findDtoById(Long id);

    Category findById(Long id);

    Page<CategoryOutputDto> findAll(Map<String, String> params);

    Page<CategoryOutputDto> findRootParents(Map<String, String> params);

    boolean existsByName(String name);

    void deleteCategory(Long id);

    CategoryOutputDto createCategory(CategoryInputDto categoryInputDto);
    CategoryOutputDto updateCategory(Long id, CategoryInputDto categoryInputDto);

    Optional<Category> findByName(String name);

    void deleteAll();
}
