package com.sosnovskyi.usermanagement.service;

import com.sosnovskyi.commondomain.dto.AdminInputDto;
import com.sosnovskyi.commondomain.dto.AdminOutputDto;
import com.sosnovskyi.commondomain.model.Admin;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminManagementService {
    AdminOutputDto saveAdmin(AdminInputDto adminInputDto);

    AdminOutputDto getAdmin(String email);

    AdminOutputDto updateAdmin(Long id, AdminInputDto adminInputDto);

    List<AdminOutputDto> findAllAdmins();

    void deleteAdmin(Long id);

    Admin findById(Long id);
}
