package com.sosnovskyi.usermanagement.controller;

import com.sosnovskyi.commondomain.dto.CategoryInputDto;
import com.sosnovskyi.commondomain.dto.CategoryOutputDto;
import com.sosnovskyi.usermanagement.service.CategoryManagementService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/categories")
public class CategoryController {

    private final CategoryManagementService categoryManagementService;

    public CategoryController(CategoryManagementService categoryManagementService) {
        this.categoryManagementService = categoryManagementService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<Page<CategoryOutputDto>> findAll(@RequestParam Map<String, String> params) {
        SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        Page<CategoryOutputDto> categories = categoryManagementService.findAll(params);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/root")
    public ResponseEntity<Page<CategoryOutputDto>> findRootParents(@RequestParam Map<String, String> params) {
        Page<CategoryOutputDto> categories = categoryManagementService.findRootParents(params);
        return new ResponseEntity<>(categories, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/{id}")
    public ResponseEntity<CategoryOutputDto> findOneById(@PathVariable Long id) {
        CategoryOutputDto category = categoryManagementService.findDtoById(id);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping
    public ResponseEntity<CategoryOutputDto> createCategory(@RequestBody @Valid CategoryInputDto categoryInputDto) {
        CategoryOutputDto category = categoryManagementService.createCategory(categoryInputDto);
        return new ResponseEntity<>(category, HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PutMapping("/{id}")
    public ResponseEntity<CategoryOutputDto> updateCategory(@PathVariable Long id,
                                                   @RequestBody @Valid CategoryInputDto categoryInputDto) {
        CategoryOutputDto category = categoryManagementService.updateCategory(id, categoryInputDto);
        return new ResponseEntity<>(category, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCategory(@PathVariable Long id) {
        categoryManagementService.deleteCategory(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
