package com.sosnovskyi.usermanagement.controller;

import com.sosnovskyi.commondomain.dto.FeedbackInputDto;
import com.sosnovskyi.commondomain.dto.FeedbackOutputDto;
import com.sosnovskyi.commondomain.dto.SpecialistInputDto;
import com.sosnovskyi.commondomain.dto.SpecialistOutputDto;
import com.sosnovskyi.usermanagement.service.SpecialistManagementService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("specialists")
public class SpecialistController {
    private final SpecialistManagementService specialistService;

    public SpecialistController(SpecialistManagementService specialistManagementService) {
        this.specialistService = specialistManagementService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<SpecialistOutputDto>> getAllSpecialists() {
        return new ResponseEntity<>(specialistService.findAllSpecialists(), HttpStatus.OK);
    }

    @GetMapping("/unverified")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Page<SpecialistOutputDto>> getAllUnverifiedSpecialists(
            @RequestParam Map<String, String> pagingParams) {
        return new ResponseEntity<>(
                specialistService.findAllUnverifiedSpecialists(pagingParams),
                HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<SpecialistOutputDto> getSpecialist(@PathVariable Long id) {
        return new ResponseEntity<>(specialistService.getSpecialist(id), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('SPECIALIST', 'ADMIN')")
    public ResponseEntity<SpecialistOutputDto> updateSpecialist(@PathVariable Long id,
                                                                @RequestBody SpecialistInputDto specialistInputDto) {
        return new ResponseEntity<>(
                specialistService.updateSpecialist(id, specialistInputDto),
                HttpStatus.OK);
    }

    @PostMapping("/feedbacks")
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<FeedbackOutputDto> addFeedback(@RequestBody FeedbackInputDto feedbackInputDto) {
        return new ResponseEntity<>(specialistService.addFeedback(feedbackInputDto), HttpStatus.OK);
    }

    @PutMapping("/verify/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<SpecialistOutputDto> verifySpecialist(@PathVariable Long id) {
        return new ResponseEntity<>(specialistService.verifySpecialist(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('SPECIALIST', 'ADMIN')")
    public ResponseEntity<Void> deleteSpecialist(@PathVariable Long id) {
        specialistService.deleteSpecialist(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
