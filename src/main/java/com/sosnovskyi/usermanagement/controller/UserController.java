package com.sosnovskyi.usermanagement.controller;

import com.sosnovskyi.commondomain.dto.UserInputDto;
import com.sosnovskyi.commondomain.dto.UserOutputDto;
import com.sosnovskyi.usermanagement.service.UserManagementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {
    private final UserManagementService userManagementService;

    public UserController(UserManagementService userManagementService) {
        this.userManagementService = userManagementService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('USER')")
    public ResponseEntity<List<UserOutputDto>> getAllUsers() {
        return new ResponseEntity<>(userManagementService.findAllUsers(), HttpStatus.OK);
    }

    @GetMapping("/{email}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<UserOutputDto> getUser(@PathVariable String email) {
        return new ResponseEntity<>(userManagementService.getUser(email), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<UserOutputDto> updateUser(@PathVariable Long id,
                                                    @RequestBody UserInputDto userInputDto) {
        return new ResponseEntity<>(userManagementService.updateUser(id, userInputDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('USER')")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userManagementService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
