package com.sosnovskyi.usermanagement.controller;

import com.sosnovskyi.commondomain.dto.AdminInputDto;
import com.sosnovskyi.commondomain.dto.AdminOutputDto;
import com.sosnovskyi.usermanagement.service.AdminManagementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("admins")
public class AdminController {
    private final AdminManagementService adminManagementService;

    public AdminController(AdminManagementService adminManagementService) {
        this.adminManagementService = adminManagementService;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AdminOutputDto>> getAllAdmins() {
        return new ResponseEntity<>(adminManagementService.findAllAdmins(), HttpStatus.OK);
    }

    @GetMapping("/{email}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ResponseEntity<AdminOutputDto> getAdmin(@PathVariable String email) {
        return new ResponseEntity<>(adminManagementService.getAdmin(email), HttpStatus.OK);
    }

    @PostMapping("/register")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AdminOutputDto> registerNewAdmin(@RequestBody AdminInputDto adminInputDto) {
        return new ResponseEntity<>(adminManagementService.saveAdmin(adminInputDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AdminOutputDto> updateAdmin(@PathVariable Long id,
                                                    @RequestBody AdminInputDto adminInputDto) {
        return new ResponseEntity<>(adminManagementService.updateAdmin(id, adminInputDto), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<Void> deleteAdmin(@PathVariable Long id) {
        adminManagementService.deleteAdmin(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
