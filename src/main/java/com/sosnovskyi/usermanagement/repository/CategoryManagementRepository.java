package com.sosnovskyi.usermanagement.repository;

import com.sosnovskyi.commondomain.model.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryManagementRepository extends JpaRepository<Category, Long> {
    boolean existsByName(String name);
    Optional<Category> findByName(String name);
    Page<Category> findCategoriesByParentCategoryIsNull(Pageable pageable);
}
