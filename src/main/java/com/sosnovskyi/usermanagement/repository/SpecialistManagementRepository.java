package com.sosnovskyi.usermanagement.repository;

import com.sosnovskyi.commondomain.model.Specialist;
import com.sosnovskyi.commondomain.repository.CommonSpecialistRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpecialistManagementRepository extends CommonSpecialistRepository {
    Optional<Specialist> findByEmail(String email);

    Page<Specialist> findAllByIsVerifiedIsFalse(Pageable pageable);
//    boolean existsByEmail(String email);
}
