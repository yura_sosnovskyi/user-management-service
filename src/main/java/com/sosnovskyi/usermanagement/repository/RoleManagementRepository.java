package com.sosnovskyi.usermanagement.repository;

import com.sosnovskyi.commondomain.model.Role;
import com.sosnovskyi.commondomain.repository.CommonRoleRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleManagementRepository extends CommonRoleRepository {
    Optional<Role> findByName(String name);
}
