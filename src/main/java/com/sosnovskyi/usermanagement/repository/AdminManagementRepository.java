package com.sosnovskyi.usermanagement.repository;

import com.sosnovskyi.commondomain.model.Admin;
import com.sosnovskyi.commondomain.repository.CommonAdminRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminManagementRepository extends CommonAdminRepository {
    Optional<Admin> findByEmail(String email);
    boolean existsByEmail(String email);
}
