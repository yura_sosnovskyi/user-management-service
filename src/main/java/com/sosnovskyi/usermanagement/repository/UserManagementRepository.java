package com.sosnovskyi.usermanagement.repository;

import com.sosnovskyi.commondomain.model.User;
import com.sosnovskyi.commondomain.repository.CommonUserRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserManagementRepository extends CommonUserRepository {
    Optional<User> findByEmail(String email);
    boolean existsByEmail(String email);
    List<User> findByLastVisitedSpecialistsId(Long specialistId);
}
